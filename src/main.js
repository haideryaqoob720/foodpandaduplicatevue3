import { createApp } from 'vue'
import App from './App.vue'
import './assets/main.js'

createApp(App).mount('#app')
